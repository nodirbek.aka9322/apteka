<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Document;
use app\modules\admin\models\DocumentSearch;
use app\modules\admin\models\DocumentStatistics;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BalanceController implements the CRUD actions for Balance model.
 */
class DocumentStatisticsController extends BaseController
{
    /**
     * @inheritDoc
     */

    /**
     * Lists all Balance models.
     *
     * @return string
     */
   
    /**
     * Lists all Balance models.
     *
     * @return string
     */
    public function actionStatistics()
    {
        $searchModel = new DocumentStatistics();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('statistics', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

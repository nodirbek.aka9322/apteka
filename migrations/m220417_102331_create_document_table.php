<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department}}`
 */
class m220417_102331_create_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document}}', [
            'id' => $this->primaryKey(),
            'document_number' => $this->string()->notNull(),
            'date' => $this->date(),
            'department_id' => $this->integer(),
            'status' => $this->tinyInteger(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-document-department_id}}',
            '{{%document}}',
            'department_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-document-department_id}}',
            '{{%document}}',
            'department_id',
            '{{%department}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-document-department_id}}',
            '{{%document}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-document-department_id}}',
            '{{%document}}'
        );

        $this->dropTable('{{%document}}');
    }
}

<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Document;
use app\modules\admin\models\DocumentItems;
use app\modules\admin\models\Balance;
use app\modules\admin\models\DocumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Exception;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentOutcomeController extends BaseController
{
    /**
     * @inheritDoc
     */

    /**
     * Lists all Document models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, Document::DOCUMENT_TYPE_OUTCOME);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'document_items' => $document_items
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */

    public function actionCreate()
    {
        $model = new Document();
        $model->scenario = Document::SCENARIO_OUTCOME;
        $model->document_type = $model::DOCUMENT_TYPE_OUTCOME;

        if ($model->load($this->request->post())) {
            try {
                $transaction = \Yii::$app->db->beginTransaction();
                $model->save();
                
                foreach ($this->request->post('DocumentItems') as $key => $value) {
                    $documentItem = new DocumentItems();
                    $documentItem->product_id = $value['product_id'];
                    $documentItem->quantity = $value['quantity'];
                    $documentItem->document_id = $model->id;
                    $documentItem->save();

                    $balance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->from_department])->one();
                    if ($balance != null && $balance->quantity >= $value['quantity']) {
                        $balance->quantity -= $value['quantity'];
                    } else {
                        throw new NotFoundHttpException('There is not enough items in the balance');
                    }
                    $balance->save();
                }

                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);
            } catch (Exception $e) {
                $transaction->rollBack();
            }


            
        } else {
            $model->loadDefaultValues();
        }

        $model->document_number = 'doc-' . date('dHis');

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Document::SCENARIO_OUTCOME;
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            try {
                $transaction = \Yii::$app->db->beginTransaction();
                foreach ($this->request->post('DocumentItems') as $key => $value) {
                    $documentItem = DocumentItems::find()->andwhere(['document_id' => $model->id])->andWhere(['product_id' => $value['product_id']])->one();
                    $balance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->from_department])->one();
                    if ($documentItem && $balance && $balance->quantity >= $value['quantity']) {
                        $balance->quantity += $documentItem->quantity;
                        $balance->quantity -= $value['quantity'];
                        $documentItem->quantity = $value['quantity'];
                    } elseif ($balance && $documentItem = null && $balance->quantity >= $value['quantity']) {
                        $documentItem = new DocumentItems();
                        $documentItem->product_id = $value['product_id'];
                        $documentItem->quantity = $value['quantity'];
                        $documentItem->document_id = $model->id;
                        $balance->quantity -= $value['quantity']; 
                    } elseif ($balance = null && $documentItem = null && $balance->quantity < $value['quantity']) {
                        throw new NotFoundHttpException('There is not enough items in the balance');
                    }
                    $balance->save();
                    $documentItem->save();
                }
            $transaction->commit();

            }   catch (Exception $e) { $transaction->rollBack(); }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'document_items' => $document_items
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();
        $model = $this->findModel($id);

        try {
            $transaction = \Yii::$app->db->beginTransaction();
            foreach ($document_items as $key => $value) {             
                $balance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->from_department])->one();
                $balance->quantity += $value['quantity'];
                $balance->save();   
            }
            $transaction->commit();

        }   catch (Exception $e) { $transaction->rollBack(); }


        $model->status = Document::DELETED;

        $model->save();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::find()->andWhere(['id' => $id])->andWhere(['!=', 'status', Document::DELETED])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

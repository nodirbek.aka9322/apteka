<?php


namespace app\components;


use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\helpers\Url;

class ExcelExport
{
    public function export($excelName){
        $filePath = Url::to('@app/web/document/templates/'.$excelName.'.xls');
        $objPHPExcel = IOFactory::load($filePath);
        return $objPHPExcel;
    }
}
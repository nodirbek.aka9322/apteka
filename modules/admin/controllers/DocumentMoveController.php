<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Document;
use app\modules\admin\models\DocumentItems;
use app\modules\admin\models\Balance;
use app\modules\admin\models\DocumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Exception;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentMoveController extends BaseController
{
    /**
     * @inheritDoc
     */

    /**
     * Lists all Document models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, Document::DOCUMENT_TYPE_MOVE);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'document_items' => $document_items
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */

    public function actionCreate()
    {
        $model = new Document();
        $model->scenario = Document::SCENARIO_MOVE;

        if ($this->request->isPost) {
            try {
                $transaction = \Yii::$app->db->beginTransaction();
                $model->document_type = $model::DOCUMENT_TYPE_MOVE;
                if ($model->load($this->request->post()) && $model->save()) {
                    foreach ($this->request->post('DocumentItems') as $key => $value) {
                        $documentItem = new DocumentItems();
                        $documentItem->product_id = $value['product_id'];
                        $documentItem->quantity = $value['quantity'];
                        $documentItem->document_id = $model->id;
                        $documentItem->save();

                        $fromBalance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->from_department])->one();
                        $toBalance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->to_department])->one();
                        if ($fromBalance != null && $fromBalance->quantity >= $value['quantity'] && $toBalance != null) {
                            $fromBalance->quantity -= $value['quantity'];
                            $toBalance->quantity += $value['quantity'];
                            $toBalance->department_id = $model->to_department;
                            $toBalance->status = $model->status;
                        } elseif ($fromBalance != null && $fromBalance->quantity >= $value['quantity']) {
                            $fromBalance->quantity -= $value['quantity'];
                            $fromBalance->department_id = $model->from_department;
                            $toBalance = new Balance();
                            $toBalance->product_id = $value['product_id'];
                            $toBalance->quantity = $value['quantity'];
                            $toBalance->document_id = $model->id;
                            $toBalance->department_id = $model->to_department;
                            $toBalance->status = $model->status;
                        } elseif ($fromBalance = null || $fromBalance->quantity < $value['quantity']) {
                            throw new NotFoundHttpException('There is not enough items in the balance');
                        }
                        $fromBalance->save();
                        $toBalance->save();
                    }
                }
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id, 'from_department' => $model->from_department, 'to_department' => $model->to_department]);
            } catch (Exception $e) {
                $transaction->rollBack();
            }


            
        } else {
            $model->loadDefaultValues();
        }

        $model->document_number = 'doc-' . date('dHis');

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Document::SCENARIO_MOVE;
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            try {
                $transaction = \Yii::$app->db->beginTransaction();
                foreach ($this->request->post('DocumentItems') as $key => $value) {
                    $documentItem = DocumentItems::find()->andwhere(['document_id' => $model->id])->andWhere(['product_id' => $value['product_id']])->one();
                    $fromBalance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->from_department])->one();
                    $toBalance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->to_department])->one();
                    if ($documentItem && $fromBalance && $toBalance) {                    
                        $fromBalance->quantity += $documentItem->quantity;
                        $fromBalance->quantity -= $value['quantity'];
                        $toBalance->quantity -= $documentItem->quantity;
                        $toBalance->quantity += $value['quantity'];
                    } elseif ($documentItem && $fromBalance && $toBalance = null) {
                        $fromBalance->quantity += $documentItem->quantity;
                        $fromBalance->quantity -= $value['quantity'];
                        $toBalance = new Balance();
                        $toBalance->product_id = $value['product_id'];
                        $toBalance->quantity = $value['quantity'];
                        $toBalance->document_id = $model->id;
                        $toBalance->department_id = $model->to_department;
                        $toBalance->status = $model->status;
                    } else {
                         throw new NotFoundHttpException('There is not enough items in the balance');
                    }
                    $fromBalance->save();
                    $toBalance->save();
                    $documentItem->save();
                }
            $transaction->commit();

            }   catch (Exception $e) { $transaction->rollBack(); }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'document_items' => $document_items
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();
        $model = $this->findModel($id);

        try {
            $transaction = \Yii::$app->db->beginTransaction();
            foreach ($document_items as $key => $value) {             
                $fromBalance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->from_department])->one();
                $toBalance = Balance::find()->andWhere(['product_id' => $value['product_id']])->andwhere(['department_id' => $model->to_department])->one();
                $fromBalance->quantity += $value['quantity'];
                $toBalance->quantity -= $value['quantity'];
                $fromBalance->save();   
                $toBalance->save();   
            }
            $transaction->commit();

        }   catch (Exception $e) { $transaction->rollBack(); }


        $model->status = Document::DELETED;

        $model->save();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::find()->andWhere(['id' => $id])->andWhere(['!=', 'status', Document::DELETED])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

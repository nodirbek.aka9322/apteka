<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%unit}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m220416_120348_create_unit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%unit}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->unique()->notNull(),
            'status' => $this->tinyInteger()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-unit-created_by}}',
            '{{%unit}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-unit-created_by}}',
            '{{%unit}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-unit-updated_by}}',
            '{{%unit}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-unit-updated_by}}',
            '{{%unit}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-unit-created_by}}',
            '{{%unit}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-unit-created_by}}',
            '{{%unit}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-unit-updated_by}}',
            '{{%unit}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-unit-updated_by}}',
            '{{%unit}}'
        );

        $this->dropTable('{{%unit}}');
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Product;
use app\modules\admin\models\Unit;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="multiple-inputs" data-count="1">
    	<div class="row">
	    	<div class="col-5 select-product">
	    		<select name="Product[0][name]"  class="form-control select_product">
					<option value=""></option>
					<option value="0">tabletka</option>
					<option value="1">dori</option>
				</select>
	    	</div>
	    	<div class="col-5">
	    		<input type="text" name="Product[0][count]" class="form-control">
	    	</div>
	    	<div class="col-2">
	    		<span type="" class="btn btn-success add-button">+</span>
	    	</div>
    	</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$js = <<< JS
    $('body').delegate('.add-button', 'click', function(e){
    	let count = $(this).parents('.multiple-inputs').attr('data-count');
    	let html = `<div class="row mt-3">
	    	<div class="col-5">
		    	<select name="Product[`+ count +`][name]"  class="form-control select_product">
					<option value=""></option>
					<option value="0">tabletka</option>
					<option value="1">dori</option>
				</select>
	    	</div>
	    	<div class="col-5">
	    		<input type="text" name="Product[`+ count +`][count]" class="form-control">
	    	</div>
	    	<div class="col-2">
	    		<span type="" class="btn btn-danger delete">-</span>
	    	</div>
    	</div>`;

    	$(this).parents('.multiple-inputs').attr('data-count', count*1 + 1);
    	$('.multiple-inputs').html($('.multiple-inputs').html() + html);
    })

    $('body').delegate('.delete', 'click', function(e){
    	$(this).parents('.row').remove();
    })
JS;

$this->registerJs($js);
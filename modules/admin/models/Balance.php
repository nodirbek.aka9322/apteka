<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\User;

/**
 * This is the model class for table "balance".
 *
 * @property int $id
 * @property int|null $product_id
 * @property float|null $quantity
 * @property int|null $document_id
 * @property int|null $department_id
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Document $document
 * @property Product $product
 */
class Balance extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'document_id', 'status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['product_id', 'department_id', 'document_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['quantity'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'document_id' => 'Document ID',
            'department_id' => 'Department ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Document]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    public function getDepartmentName()
    {
        $department= Department::findOne(['id' => $this->department_id]);
                    
        if ($department) {
            return $department->name;
        }

        return '';
    }

    public function getCreatorName()
    {
        $user = User::findOne(['id' => $this->created_by]);
                    
        if ($user) {
            return $user->name;
        }

        return '';
    }

    public function getUpdatorName()
    {
        $user = User::findOne(['id' => $this->updated_by]);
                    
        if ($user) {
            return $user->name;
        }

        return '';
    }
}

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\models\Document;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Document Sale', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'document_number',
            'date',
            [
                'attribute' => 'from_department',
                'value' => function($model) {
                    return $model->fromDepartment->name;
                }
            ],
            [
                'attribute'  => 'status',
                'value'  => function($model) {
                    return $model->getStatusName();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    $model->created_at =date('d.m.Y H:i:s', strtotime($model->created_at));
                    return $model->created_at;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    $model->updated_at =date('d.m.Y H:i:s', strtotime($model->updated_at));
                    return $model->updated_at;
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Document $model, $key, $index, $column) {
                    if ($action === 'view') {
                        $url ='/admin/document-sale/view?id='.$model->id;
                        return $url;
                    }

                    if ($action === 'update') {
                        $url ='/admin/document-sale/update?id='.$model->id;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='/admin/document-sale/delete?id='.$model->id;
                        return $url;
                    }

                  }
              ],


            
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%region}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%region}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m220414_083016_create_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->tinyInteger()->unique()->notNull(),
            'parent_id' => $this->tinyInteger(),
            'status' => $this->tinyInteger()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `parent_id`
        $this->createIndex(
            '{{%idx-region-parent_id}}',
            '{{%region}}',
            'parent_id'
        );

        // add foreign key for table `{{%region}}`
        $this->addForeignKey(
            '{{%fk-region-parent_id}}',
            '{{%region}}',
            'parent_id',
            '{{%region}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-region-created_by}}',
            '{{%region}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-region-created_by}}',
            '{{%region}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-region-updated_by}}',
            '{{%region}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-region-updated_by}}',
            '{{%region}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%region}}`
        $this->dropForeignKey(
            '{{%fk-region-parent_id}}',
            '{{%region}}'
        );

        // drops index for column `parent_id`
        $this->dropIndex(
            '{{%idx-region-parent_id}}',
            '{{%region}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-region-created_by}}',
            '{{%region}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-region-created_by}}',
            '{{%region}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-region-updated_by}}',
            '{{%region}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-region-updated_by}}',
            '{{%region}}'
        );

        $this->dropTable('{{%region}}');
    }
}

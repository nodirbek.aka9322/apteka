<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\DocumentItems;
use app\modules\admin\models\User;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property string $document_number
 * @property string|null $date
 * @property int|null $from_department
 * @property int|null $to_department
 * @property int|null $document_type
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Department $department
 * @property DocumentItems[] $documentItems
 */
class Document extends BaseModel
{
    const DOCUMENT_TYPE_INCOME  = 1;
    const DOCUMENT_TYPE_OUTCOME = 2;
    const DOCUMENT_TYPE_MOVE    = 3;

    const SCENARIO_INCOME   = 'income';
    const SCENARIO_MOVE     = 'move';
    const SCENARIO_OUTCOME  = 'outcome';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_number'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['from_department', 'to_department', 'status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['from_department', 'to_department', 'document_type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['document_number'], 'string', 'max' => 255],
            [['from_department',],'required','on'=>[self::SCENARIO_OUTCOME, self::SCENARIO_MOVE]],
            [['to_department',],'required','on'=>[self::SCENARIO_INCOME, self::SCENARIO_MOVE]],
            [['from_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['from_department' => 'id']],
            [['to_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['to_department' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document_number' => 'Document Number',
            'date' => 'Date',
            'from_department' => 'From Department',
            'to_department' => 'To Department',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Department]].
     *
     * @return \yii\db\ActiveQuery
     */
    

    public function getFromDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'from_department']);
    }

    public function getToDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'to_department']);
    }

    public function saveItems($documentItems, $update = false)
    {
        foreach ($documentItems as $key => $value) {
            
            $this->saveDocumentItem($value['product_id'], $value['quantity'], $update);

        }
    }

    private function saveDocumentItem($product_id, $quantity, $update)
    {
        $quantity_balance = $quantity;

        if (!$update) {
            $documentItem = new DocumentItems();
            $documentItem->product_id = $product_id;
            $documentItem->document_id = $this->id;
        }

        if ($update) {
            $documentItem = DocumentItems::find()
                ->andwhere(['document_id' => $this->id])
                ->andWhere(['product_id' => $product_id])
                ->one();

            if ($documentItem) {
                $quantity_balance = $quantity - $documentItem->quantity;
            } else {
                $documentItem = new DocumentItems();
                $documentItem->product_id = $product_id;
                $documentItem->document_id = $this->id;
            }
        }
        $documentItem->quantity = $quantity;
        
        $documentItem->save();
        
        $this->saveBalance($product_id, $quantity_balance, $update);
    }

    private function saveBalance($product_id, $quantity_balance, $update)
    {
        $balance = Balance::find()
            ->andWhere(['product_id' => $product_id])
            ->andwhere(['department_id' => $this->to_department])
            ->one();

        if ($balance) {
            $balance->quantity += $quantity_balance;
        } else {
            $balance = new Balance();
            $balance->product_id = $product_id;
            $balance->quantity = $quantity_balance;
            $balance->department_id = $this->to_department;
            $balance->status = Balance::ACTIVE;
        }
        
        $balance->save();
    }

    public function getFromDepartmentName()
    {
        $department = Department::findOne(['id' => $this->from_department]);
                    
        if ($department) {
            return $department->name;
        }

        return '';
    }

    public function getToDepartmentName()
    {
        $department = Department::findOne(['id' => $this->to_department]);
                    
        if ($department) {
            return $department->name;
        }

        return '';
    }
    public function getCreatorName()
    {
        $user = User::findOne(['id' => $this->created_by]);
                    
        if ($user) {
            return $user->name;
        }

        return '';
    }

    /**
     * Gets query for [[DocumentItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItems()
    {
        return $this->hasMany(DocumentItems::className(), ['document_id' => 'id']);
    }

    public function getDocumentItem()
    {
        return $documentItem = DocumentItems::find(['document_id' => $this->id])->all();
    }

    public function getUpdatorName()
    {
        $user = User::findOne(['id' => $this->updated_by]);
                    
        if ($user) {
            return $user->name;
        }

        return '';
    }
}

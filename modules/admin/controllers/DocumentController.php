<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Document;
use app\modules\admin\models\DocumentItems;
use app\modules\admin\models\Balance;
use app\modules\admin\models\DocumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Exception;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends BaseController
{
    /**
     * @inheritDoc
     */

    /**
     * Lists all Document models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, Document::DOCUMENT_TYPE_INCOME);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Document models.
     *
     * @return string
     */

    /**
     * Displays a single Document model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'document_items' => $document_items
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Document();
        $model->scenario = Document::SCENARIO_INCOME;
            $model->document_type = $model::DOCUMENT_TYPE_INCOME;

        if ($model->load($this->request->post())) {
            try {
                $transaction = \Yii::$app->db->beginTransaction();

                $model->save();
                $model->saveItems($this->request->post('DocumentItems'));

                $transaction->commit();

            } catch (Exception $e) { 
                $transaction->rollBack(); 
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->loadDefaultValues();
        }

        $model->document_number = 'doc-' . date('dHis');

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Document::SCENARIO_INCOME;
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();

        if ($this->request->isPost && $model->load($this->request->post())) {
            try {
                $transaction = \Yii::$app->db->beginTransaction();

                $model->save();
                $model->saveItems($this->request->post('DocumentItems'), true);

                $transaction->commit();

            }   catch (Exception $e) { 
                $transaction->rollBack(); 
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'document_items' => $document_items
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
        $document_items = DocumentItems::find()->with(['product'])->andWhere(['document_id' => $id])->all();
        $model = $this->findModel($id);
        

        try {
            $transaction = \Yii::$app->db->beginTransaction();
            foreach ($document_items as $key => $value) {             
                $balance = Balance::find()
                    ->andWhere(['product_id' => $value['product_id']])
                    ->andwhere(['department_id' => $model->to_department])
                    ->one();
                    
                if ($balance->quantity >= $value['quantity']) {
                        $balance->quantity -= $value['quantity'];
                    } else {
                        throw new NotFoundHttpException('There is not enough items in the balance');
                    }
                $balance->save();   
            }
            $transaction->commit();

        }   catch (Exception $e) { $transaction->rollBack(); }


        $model->status = Document::DELETED;

        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::find()->andWhere(['id' => $id])->andWhere(['!=', 'status', Document::DELETED])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

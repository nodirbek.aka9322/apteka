<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%document}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department}}`
 */
class m220420_121545_add_to_department_column_to_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%document}}', 'to_department', $this->integer());

        // creates index for column `to_department`
        $this->createIndex(
            '{{%idx-document-to_department}}',
            '{{%document}}',
            'to_department'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-document-to_department}}',
            '{{%document}}',
            'to_department',
            '{{%department}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-document-to_department}}',
            '{{%document}}'
        );

        // drops index for column `to_department`
        $this->dropIndex(
            '{{%idx-document-to_department}}',
            '{{%document}}'
        );

        $this->dropColumn('{{%document}}', 'to_department');
    }
}

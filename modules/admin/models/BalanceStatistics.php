<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Balance;

/**
 * BalanceSearch represents the model behind the search form of `app\modules\admin\models\Balance`.
 */
class BalanceStatistics extends Balance
{
    public $from_date;
    public $to_date;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from_date', 'to_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Balance::find()->andWhere(['!=', 'status', Balance::DELETED]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        return null;
        if (!$this->validate()) {
            return null;
        }

        $query->andFilterWhere(['>', 'created_at', $this->from_date])
            ->andFilterWhere(['<', 'created_at', $this->to_date]);

        return $dataProvider;
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'username',
            [
                'attribute'  => 'role',
                'value'  => function($model) {
                    return $model->getRoleName();
                }
            ],
            'email:email',
            [
                'attribute'  => 'status',
                'value'  => function($model) {
                    return $model->getStatusName();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    $model->created_at =date('d.m.Y H:i:s', strtotime($model->created_at));
                    return $model->created_at;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    $model->updated_at =date('d.m.Y H:i:s', strtotime($model->updated_at));
                    return $model->updated_at;
                }
            ],
        ],
    ]) ?>

</div>

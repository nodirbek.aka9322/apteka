<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%document}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department}}`
 */
class m220420_121529_add_from_department_column_to_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%document}}', 'from_department', $this->integer());

        // creates index for column `from_department`
        $this->createIndex(
            '{{%idx-document-from_department}}',
            '{{%document}}',
            'from_department'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-document-from_department}}',
            '{{%document}}',
            'from_department',
            '{{%department}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-document-from_department}}',
            '{{%document}}'
        );

        // drops index for column `from_department`
        $this->dropIndex(
            '{{%idx-document-from_department}}',
            '{{%document}}'
        );

        $this->dropColumn('{{%document}}', 'from_department');
    }
}

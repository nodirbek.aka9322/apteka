<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'code',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getStatusName();
                }
            ],
            [
                'attribute' => 'unit',
                'value' => function($model) {
                    return $model->unit0->name;
                }
            ],
            'piece',
            'price',
            'remainder',
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    $model->created_at =date('d.m.Y H:i:s', strtotime($model->created_at));
                    return $model->created_at;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    $model->updated_at =date('d.m.Y H:i:s', strtotime($model->updated_at));
                    return $model->updated_at;
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function($model) {
                    return $model->getCreatorName();
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function($model) {
                    return $model->getUpdatorName();
                }
            ],
        ],
    ]) ?>

</div>

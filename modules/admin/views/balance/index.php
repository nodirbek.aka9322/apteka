<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\models\Balance;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use yii\db\ActiveRecord;
use kartikorm\ActiveForm;
use app\modules\admin\views\BaseIndex;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\BalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Balances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="balance-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Balance', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'product_id',
                'value' => function($model) {
                    return $model->product->name;
                }
            ],
            'quantity',
            [
                'attribute' => 'department_id',
                'value' => function($model) {
                    return $model->getDepartmentName();
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

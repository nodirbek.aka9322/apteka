<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%document}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department}}`
 */
class m220420_221348_drop_department_id_column_from_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-document-department_id}}',
            '{{%document}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-document-department_id}}',
            '{{%document}}'
        );

        $this->dropColumn('{{%document}}', 'department_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%document}}', 'department_id', $this->integer());

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-document-department_id}}',
            '{{%document}}',
            'department_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-document-department_id}}',
            '{{%document}}',
            'department_id',
            '{{%department}}',
            'id',
            'CASCADE'
        );
    }
}

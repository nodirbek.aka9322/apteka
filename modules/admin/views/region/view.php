<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\admin\models\User;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\grid\ActionColumn;
use app\modules\admin\models\Region;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['index']];
$parents = Region::find()->andWhere(['!=', 'status', Region::DELETED]);
if ($model->parent_id) {
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['view?id=' . $model->parent_id]];    
}
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($model->parent_id): ?>
            <?= Html::a('Back', ['view', 'id' => $model->parent_id], ['class' => 'btn btn-info']) ?>
        <?php else: ?>
            <?= Html::a('Back', ['index'], ['class' => 'btn btn-info']) ?>
        <?php endif ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add child', ['add-region', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'code',
            [
                'attribute' => 'parent_id',
                'value' => function($model) {
                    if ($model->parent_id) {
                        return $model->parent->name;
                    }
                    return 'Main Region';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getStatusName();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    $model->created_at =date('d.m.Y H:i:s', strtotime($model->created_at));
                    return $model->created_at;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    $model->updated_at =date('d.m.Y H:i:s', strtotime($model->updated_at));
                    return $model->updated_at;
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function($model) {
                    return $model->getCreatorName();
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function($model) {
                    return $model->getUpdatorName();
                }
            ],
        ],
    ]) ?>



    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'code',
            // 'parent_id',
            [
                'attribute' => 'parent_id',
                'format' => 'raw',
                'value' => function($model) {
                    return "<a href='" . Url::to(['region/add-region', 'id' => $model->id]) . "'>Add Region</a>";
                }
            ],
            [
                'attribute'  => 'status',
                'value'  => function($model) {
                    return $model->getStatusName();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    $model->created_at =date('d.m.Y H:i:s', strtotime($model->created_at));
                    return $model->created_at;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    $model->updated_at =date('d.m.Y H:i:s', strtotime($model->updated_at));
                    return $model->updated_at;
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function($model) {
                    return $model->getCreatorName();
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function($model) {
                    return $model->getUpdatorName();
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Region $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

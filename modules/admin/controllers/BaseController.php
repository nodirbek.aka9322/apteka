<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use app\modules\admin\models\User;
/**
 * CategoryController implements the CRUD actions for Category model.
 */
class BaseController extends Controller
{

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * @param  yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $url = \Yii::$app->controller->id . "/" . \Yii::$app->controller->action->id; // user/index, products/view
        if (\Yii::$app->user->identity->can($url)) {
            return parent::beforeAction($action);
        }
        
        throw new ForbiddenHttpException(\Yii::t('app', 'Access denied'));
        
    }

}
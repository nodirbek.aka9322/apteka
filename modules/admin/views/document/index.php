<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\models\Document;
use app\modules\admin\models\User;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Document Income', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'document_number',
            'date',
            [
                'attribute' => 'to_department',
                'value' => function($model) {
                    return $model->toDepartment->name;
                }
            ],
            //'created_at',
            //'updated_at',
            [
                'attribute'  => 'status',
                'value'  => function($model) {
                    return $model->getStatusName();
                }
            ],
            //'created_at',
            //'updated_at',
            [
                'attribute' => 'created_by',
                'value' => function($model) {
                    return $model->getCreatorName();
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function($model) {
                    return $model->getUpdatorName();
                }
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {update} {delete}',
                'options' => ['style' => 'width: 88px;'],
                'visibleButtons' => [
                    'view' => \Yii::$app->user->identity->can('document/view'),
                    'update' => \Yii::$app->user->identity->can('document/update'),
                    'delete' => \Yii::$app->user->identity->can('document/delete')
                ],
                'urlCreator' => function ($action, Document $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>






<tr>
                                    <th>#</th>
                                    <th>
                                        <input type="hidden" value="<?= $balance->product_id?>" name="Balance[<?= $key?>][product_id]" class="form-control product-value">
                                        <input type="text" value="<?= $balance->product->name ?>" class="form-control" disabled>
                                    </th>
                                    <th>
                                        <input type="text" value="<?= $balance->quantity ?>" name="Balance[<?= $key?>][quantity]" class="form-control" disabled>
                                    </th>
                                    <th>
                                        <input type="hidden" value="<?= $balance->department_id?>" name="Balance[<?= $key?>][product_id]" class="form-control product-value">
                                        <input type="text" value="<?= $balance->department->name ?>" class="form-control" disabled>
                                    </th>
                                </tr>

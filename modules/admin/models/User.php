<?php

namespace app\modules\admin\models;

use Yii;
use app\helpers\PermitionUrlLists;


/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $role
 * @property string|null $email
 * @property string $password
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Region[] $regions
 * @property Region[] $regions0
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface, PermitionUrlLists
{
    const ADMIN = 1;
    const USER  = 2;
    const SALER = 3;
    
    const ACTIVE    = 1;
    const IN_ACTIVE = 2;
    const DELETED   = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'username', 'role', 'password', 'status'], 'required'],
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'username', 'role', 'email', 'password'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'role' => 'Role',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Regions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::className(), ['created_by' => 'id']);
    }

    // *
    //  * Gets query for [[Regions0]].
    //  *
    //  * @return \yii\db\ActiveQuery
     
    public function getRegions0()
    {
        return $this->hasMany(Region::className(), ['updated_by' => 'id']);
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }


    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public static function getStatusList()
    {
        return [
            self::ACTIVE => 'Active',
            self::IN_ACTIVE => 'Inactive'
        ];
    }

    public function getStatusName()
    {
        $statusList = self::getStatusList();

        if (array_key_exists($this->status, $statusList)) {
            return $statusList[$this->status];
        }
        return '';
    }

    public static function getRoleList()
    {
        return [
            self::ADMIN => 'ADMIN',
            self::USER  => 'USER',
            self::SALER => 'SALER',
        ];
    }

    public function can($url)
    {
        if (User::isUser() && !array_key_exists($url, self::user_actions)) {
            return false;
        } elseif (User::isSaler() && !array_key_exists($url, self::saler_actions)) {
            return false;
        }
        return true;
    }

    public function getRoleName()
    {
        $roleList = self::getRoleList();

        if (array_key_exists($this->role, $roleList)) {
            return $roleList[$this->role];
        }
        return '';
    }

    public static function isAdmin()
    {
        return self::cheackRole(self::ADMIN);
    }

    public static function isUser()
    {
        return self::cheackRole(self::USER);
    }

    public static function isSaler()
    {
        return self::cheackRole(self::SALER);
    }

    private static function cheackRole($role)
    {
        return !Yii::$app->user->isGuest && Yii::$app->user->identity->role  == $role;
    }
}

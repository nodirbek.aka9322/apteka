<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Balance;

/* @var $this yii\web\View */

$this->title = 'Balances statistics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="balance-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'method' => 'get'
    ]); ?>
    <div class="row">
        <div class="col-3">
            
        </div>
        <div class="col-6">
            <p>

                
                <?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'from_date',
                    'attribute2' => 'to_date',
                    'options' => ['placeholder' => 'Start date'],
                    'options2' => ['placeholder' => 'End date'],
                    'type' => DatePicker::TYPE_RANGE,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                    ]
                ]);
                
                // debug($balances);
                ?>
                
            </p>        
        </div>
        <div class="col-3 pt-3">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>    
        </div>
                
    </div>
    <div class="row">
        <div class="multiple-inputs">
            <div class="col-2">
                
            </div>
            <?php $balances = Balance::find()->andFilterWhere(['>', 'created_at', $searchModel->from_date])
                                            ->andFilterWhere(['<', 'created_at', $searchModel->to_date])
                                            ->andWhere(['!=', 'status', Balance::DELETED])
                                            ->all(); ?>

            
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table-bordered table hide_table offcanvas">
                                <thead class="table_head">
                                <tr>
                                    <th>#</th>
                                    <th><?php echo Yii::t('app', 'Product name') ?></th>
                                    <th><?php echo Yii::t('app', 'Quantity') ?></th>
                                    <th><?php echo Yii::t('app', 'Department') ?></th>
                                </tr>
                                </thead>
                                <tbody class="table_body">
                                <?php if (isset($balances)): ?>
                                <?php foreach ($balances as $key => $balance): ?>
                                <tr>
                                    <th>#</th>
                                    <th>
                                        <input type="hidden" value="<?= $balance->product_id?>" name="Balance[<?= $key?>][product_id]" class="form-control product-value">
                                        <input type="text" value="<?= $balance->product->name ?>" class="form-control" disabled>
                                    </th>
                                    <th>
                                        <input type="text" value="<?= $balance->quantity ?>" name="Balance[<?= $key?>][quantity]" class="form-control" disabled>
                                    </th>
                                    <th>
                                        <input type="hidden" value="<?= $balance->department_id?>" name="Balance[<?= $key?>][product_id]" class="form-control product-value">
                                        <input type="text" value="<?= $balance->department->name ?>" class="form-control" disabled>
                                    </th>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                    
                
        </div>
        <div class="col-2">
                    
        </div>        
    </div>
    <?php ActiveForm::end(); ?>
    
</div>

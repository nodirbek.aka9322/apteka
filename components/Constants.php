<?php


namespace app\components;


class Constants
{
    public static function getModulesArray(){
        return [
            'admin' => 'admin',
            'manuals' => 'manuals',
            'warehouse' => 'warehouse',
            'structure' => 'structure',
            'terminal' => 'terminal',
            'finance' => 'finance',
        ];
    }

}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%department}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m220416_103121_create_department_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->unique()->notNull(),
            'parent_id' => $this->tinyInteger(),
            'status' => $this->tinyInteger()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `parent_id`
        $this->createIndex(
            '{{%idx-department-parent_id}}',
            '{{%department}}',
            'parent_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-department-parent_id}}',
            '{{%department}}',
            'parent_id',
            '{{%department}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-department-created_by}}',
            '{{%department}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-department-created_by}}',
            '{{%department}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-department-updated_by}}',
            '{{%department}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-department-updated_by}}',
            '{{%department}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-department-parent_id}}',
            '{{%department}}'
        );

        // drops index for column `parent_id`
        $this->dropIndex(
            '{{%idx-department-parent_id}}',
            '{{%department}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-department-created_by}}',
            '{{%department}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-department-created_by}}',
            '{{%department}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-department-updated_by}}',
            '{{%department}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-department-updated_by}}',
            '{{%department}}'
        );

        $this->dropTable('{{%department}}');
    }
}

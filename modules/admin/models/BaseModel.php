<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\behaviors\CommonBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string|null $massage
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class BaseModel extends \yii\db\ActiveRecord
{
    const ACTIVE    = 1;
    const IN_ACTIVE = 2;
    const DELETED   = 3;


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => date('Y-m-d H:i:s')
            ],
            [
                'class' => CommonBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],

            ],
        ];
    }

    public static function getStatusList()
    {
        return [
            self::ACTIVE => 'Active',
            self::IN_ACTIVE => 'Inactive'
        ];
    }

    public function getStatusName()
    {
        $statusList = self::getStatusList();

        if (array_key_exists($this->status, $statusList)) {
            return $statusList[$this->status];
        }
        return '';
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Document */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="document-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'document_number',
            'date',
            [
                'attribute' => 'from_department',
                'value' => function($model) {
                    return $model->fromDepartment->name;
                }
            ],
            [
                'attribute'  => 'status',
                'value'  => function($model) {
                    return $model->getStatusName();
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function($model) {
                    return $model->getCreatorName();
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function($model) {
                    return $model->getUpdatorName();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    $model->created_at =date('d.m.Y H:i:s', strtotime($model->created_at));
                    return $model->created_at;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    $model->updated_at =date('d.m.Y H:i:s', strtotime($model->updated_at));
                    return $model->updated_at;
                }
            ],
        ],
    ]) ?>

    <table class="table">
        <thead>
            <tr>
                <th>Product</th>
                <th>Quantity</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($document_items as $key => $document_item): ?>
                <tr>
                    <td><?=$document_item->product->name ?></td>
                    <td><?=$document_item->quantity ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\modules\admin\models\Department;
use app\modules\admin\models\Product;
use app\modules\admin\models\Document;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Document */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="document-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        
        <div class="col-4">
        
            <?= $form->field($model, 'document_number')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Enter birth date ...'],
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]); ?>

            <?= $form->field($model, 'to_department')->widget(Select2::classname(), [
                'data' => Department::getDepartmentList(),
                'options' => ['placeholder' => 'Select a department ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>

            <?= $form->field($model, 'status')->dropDownList(Document::getStatusList()) ?>

        </div>

        <div class="col-8">

            <div class="product-form"  data-count="<?= (isset($document_items)) ? count($document_items) : 1 ?>">
                <div style="width: 50%; margin: 0 auto;" class="mb-3">
                    <?= Select2::widget([
                        'name' => 'products',
                        'data' => Product::getProductList(),
                        'options' => [
                            'class' => 'products-list'
                        ]
                    ]); ?>
                </div>

                <div class="multiple-inputs">
                    <?php if (isset($document_items)): ?>
                        <?php foreach ($document_items as $key => $document_item): ?>
                            <div class="row mt-3">
                                <div class="col-5">
                                    <input type="hidden" value="<?= $document_item->product_id?>" name="DocumentItems[<?= $key?>][product_id]" class="form-control product-value">
                                    <input type="text" value="<?= $document_item->product->name ?>" class="form-control" disabled>
                                </div>
                                <div class="col-5">
                                    <input type="text" value="<?= $document_item->quantity ?>" name="DocumentItems[<?= $key?>][quantity]" class="form-control">
                                </div>
                                <div class="col-2">
                                    <span type="" class="btn btn-danger delete">-</span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

$js = <<< JS
    $('body').delegate('.delete', 'click', function(e){
        $(this).parents('.row').remove();
    })
    
    $('body').delegate('.products-list', 'change', function(e){
        let id = $(this).val();
        let text = $('#select2-w1-container').html();
        let count = $(this).parents('.product-form').attr('data-count');
        let html = `<div class="row mt-3">
            <div class="col-5">
                <input type="hidden" value="`+id+`" name="DocumentItems[`+ count +`][product_id]" class="form-control product-value">
                <input type="text" value="`+text+`" class="form-control" disabled>
            </div>
            <div class="col-5">
                <input type="text" name="DocumentItems[`+ count +`][quantity]" class="form-control">
            </div>
            <div class="col-2">
                <span type="" class="btn btn-danger delete">-</span>
            </div>
        </div>`;

        let past = true;
        $.map( $('.product-value'), function( a ) {
            if($(a).val()*1 == id*1) {
                past = false;
            }
        });

        if(id*1 != 0 && past) {
            $(this).parents('.product-form').attr('data-count', count*1 + 1);
            $('.multiple-inputs').html($('.multiple-inputs').html() + html);    
        }
        
    })


JS;

$this->registerJs($js);
<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\modules\admin\models\User;
use app\modules\admin\models\Region;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    
    $users = [];

    if (User::isAdmin()) {
        $users = [
            ['label' => 'References', 'items' => [
                ['label' => 'Regions', 'url' => ['/admin/region/index']],
                ['label' => 'Units', 'url' => ['/admin/unit/index']],
                ['label' => 'Categories', 'url' => ['/admin/category/index']],
                ['label' => 'Departments', 'url' => ['/admin/department/index']],
                ['label' => 'Users', 'url' => ['/admin/user/index']]
            ]],
            ['label' => 'Products', 'url' => ['/admin/product/index']],
            ['label' => 'Documents', 'items' => [
                ['label' => 'Document Income', 'url' => ['/admin/document/index']],
                ['label' => 'Document Outcome', 'url' => ['/admin/document-outcome/index']],
                ['label' => 'Document Move', 'url' => ['/admin/document-move/index']],
                ['label' => 'Document Statistics', 'url' => ['/admin/document-statistics/statistics']],
            ]],
            ['label' => 'Warehouse', 'items' => [
                ['label' => 'Balance', 'url' => ['/admin/balance/index']],
                ['label' => 'Balance statistics', 'url' => ['/admin/balance/statistics']],
            ]],
        ];
    } elseif (User::isUser()) {
        $users = [
        ['label' => 'Documents', 'items' => [
            ['label' => 'Document Income', 'url' => ['/admin/document/index']],
            ['label' => 'Document Outcome', 'url' => ['/admin/document-outcome/index']],
            ['label' => 'Document Move', 'url' => ['/admin/document-move/index']],
        ]],
        ['label' => 'Balance', 'url' => ['/admin/balance/index']],
    ];
    } elseif (User::isSaler()) {
        $users = [['label' => 'Sale', 'url' => ['/admin/document-sale/create']]];
    }


    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => array_merge([
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            ),

            ['label' => 'Home', 'url' => ['/site/index']],
        ], $users )
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

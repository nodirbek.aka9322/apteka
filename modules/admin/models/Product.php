<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $status
 * @property int $unit
 * @property float|null $remainder
 * @property int|null $piece
 * @property int|null $price
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property User $createdBy
 * @property Unit $unit0
 * @property User $updatedBy
 */
class Product extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'status', 'unit', 'created_by', 'updated_by'], 'required'],
            [['status', 'unit', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['status', 'unit', 'piece', 'price', 'created_by', 'updated_by'], 'integer'],
            [['remainder'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'code'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['unit'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['unit' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'status' => 'Status',
            'unit' => 'Unit',
            'piece' => 'Piece',
            'price' => 'Price',
            'remainder' => 'Remainder',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getCreatorName()
    {
        $user = User::findOne(['id' => $this->created_by]);
                    
        if ($user) {
            return $user->name;
        }

        return '';
    }

    /**
     * Gets query for [[Unit0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit0()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit']);
    }

    public static function getProductList()
    {
        return [0 => ''] + ArrayHelper::map(self::find()->andWhere(['status' => self::ACTIVE])->all(), 'id', 'name');
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getUpdatorName()
    {
        $user = User::findOne(['id' => $this->updated_by]);
                    
        if ($user) {
            return $user->name;
        }

        return '';
    }
}

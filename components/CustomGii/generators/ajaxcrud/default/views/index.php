<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>
use yii\helpers\Url;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->viewPath)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-custom <?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>-index">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="col-lg-9 col-xl-8" style="padding: 0!important;">

        </div>
        <div class="card-toolbar">
        <?="<?php"?> if (Yii::$app->user->can('<?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>/create')): ?>
            <a href="<?= '<?=' ?> Url::to('create') ?>"
               class="btn btn-sm btn-primary font-weight-bolder create-dialog">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                    </span><?= '<?=' ?> Yii::t('app', 'Create') ?>
            </a>
        <?="<?php endif; ?>\n"?>
        </div>
    </div>
    <div class="separator separator-solid"></div>
    <div class="card-body">
        <div class="table-responsive">
<?="    <?php Pjax::begin(['id' => '".Inflector::camel2id(StringHelper::basename($generator->viewPath))."_pjax']); ?>\n"?>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            ['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            //'" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{view}{delete}',
                'contentOptions' => ['class' => 'no-print','style' => 'width:130px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('<?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('<?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('<?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pen m-0 p-0"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class'=> 'update-dialog btn btn-outline-primary btn-xs click-button-update m-0 p-2',
                            'data-form-id' => $model['id'],
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-eye m-0 p-0"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=> 'btn btn-outline-info btn-xs view-modal-show m-0 p-2 view-dialog',
                            'data-form-id' => $model['id'],
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash m-0 p-0"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-outline-danger btn-xs m-0 p-2 delete-dialog',
                            'data-form-id' => $model['id'],
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>

<?="    <?php Pjax::end(); ?>\n"?>
        </div>
    </div>
</div>
<?= "<?= " ?> \app\widgets\ModalWindow\ModalWindow::widget([
    'model' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>',
    'crud_name' => '<?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>',
    'modal_id' => '<?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>-modal',
    'modal_header' => '<h3 class="modal-title">'. Yii::t('app', '<?=Inflector::camel2words(StringHelper::basename($generator->viewPath)) ?>') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-lg',
    'grid_ajax' => '<?= Inflector::camel2id(StringHelper::basename($generator->viewPath)) ?>_pjax',
    'confirm_message' => Yii::t('app', 'Are you sure you want to delete this item?'),
    'pretty_url' => true
]); ?>

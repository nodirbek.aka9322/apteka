<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document_items}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%product}}`
 * - `{{%document}}`
 */
class m220417_102742_create_document_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document_items}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'quantity' => $this->decimal(20,3),
            'document_id' => $this->integer(),
            'status' => $this->tinyInteger(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-document_items-product_id}}',
            '{{%document_items}}',
            'product_id'
        );

        // add foreign key for table `{{%product}}`
        $this->addForeignKey(
            '{{%fk-document_items-product_id}}',
            '{{%document_items}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        // creates index for column `document_id`
        $this->createIndex(
            '{{%idx-document_items-document_id}}',
            '{{%document_items}}',
            'document_id'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-document_items-document_id}}',
            '{{%document_items}}',
            'document_id',
            '{{%document}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%product}}`
        $this->dropForeignKey(
            '{{%fk-document_items-product_id}}',
            '{{%document_items}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-document_items-product_id}}',
            '{{%document_items}}'
        );

        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-document_items-document_id}}',
            '{{%document_items}}'
        );

        // drops index for column `document_id`
        $this->dropIndex(
            '{{%idx-document_items-document_id}}',
            '{{%document_items}}'
        );

        $this->dropTable('{{%document_items}}');
    }
}

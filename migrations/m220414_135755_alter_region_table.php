<?php

use yii\db\Migration;

/**
 * Class m220414_135755_alter_region_table
 */
class m220414_135755_alter_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%region}}', 'code', $this->string()->unique()->notNull());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%region}}', 'code', $this->tinyInteger()->unique()->notNull());
    }

}

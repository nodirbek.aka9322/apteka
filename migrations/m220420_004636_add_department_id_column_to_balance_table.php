<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department}}`
 */
class m220420_004636_add_department_id_column_to_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%balance}}', 'department_id', $this->integer());

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-balance-department_id}}',
            '{{%balance}}',
            'department_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-balance-department_id}}',
            '{{%balance}}',
            'department_id',
            '{{%department}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-balance-department_id}}',
            '{{%balance}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-balance-department_id}}',
            '{{%balance}}'
        );

        $this->dropColumn('{{%balance}}', 'department_id');
    }
}

<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Balance;
use app\modules\admin\models\BalanceSearch;
use app\modules\admin\models\BalanceStatistics;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BalanceController implements the CRUD actions for Balance model.
 */
class BalanceController extends BaseController
{
    /**
     * @inheritDoc
     */

    /**
     * Lists all Balance models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new BalanceSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Balance models.
     *
     * @return string
     */
    public function actionStatistics()
    {
        $searchModel = new BalanceStatistics();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('statistics', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

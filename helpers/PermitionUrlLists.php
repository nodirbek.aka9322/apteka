<?php

namespace app\helpers;


interface PermitionUrlLists
{
    const user_actions = [
        'document/index' => 1,
        'document/create' => 1,
        'document/view' => 1,
        'document/update' => 1,

        'document-outcome/index' => 1,
        'document-outcome/create' => 1,
        'document-outcome/view' => 1,
        'document-outcome/update' => 1,

        'document-move/index' => 1,
        'document-move/create' => 1,
        'document-move/view' => 1,
        'document-move/update' => 1,

        'balance/index' => 1,
    ];
    const saler_actions = [
        'document-sale/index' => 1,
        'document-sale/create' => 1,
        'document-sale/view' => 1,
        'document-sale/update' => 1,
    ];


}
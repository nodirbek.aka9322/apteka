<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%product}}`
 * - `{{%document}}`
 */
class m220418_104951_create_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%balance}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'quantity' => $this->decimal(20,3),
            'document_id' => $this->integer(),
            'status' => $this->tinyInteger(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-balance-product_id}}',
            '{{%balance}}',
            'product_id'
        );

        // add foreign key for table `{{%product}}`
        $this->addForeignKey(
            '{{%fk-balance-product_id}}',
            '{{%balance}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        // creates index for column `document_id`
        $this->createIndex(
            '{{%idx-balance-document_id}}',
            '{{%balance}}',
            'document_id'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-balance-document_id}}',
            '{{%balance}}',
            'document_id',
            '{{%document}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%product}}`
        $this->dropForeignKey(
            '{{%fk-balance-product_id}}',
            '{{%balance}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-balance-product_id}}',
            '{{%balance}}'
        );

        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-balance-document_id}}',
            '{{%balance}}'
        );

        // drops index for column `document_id`
        $this->dropIndex(
            '{{%idx-balance-document_id}}',
            '{{%balance}}'
        );

        $this->dropTable('{{%balance}}');
    }
}
